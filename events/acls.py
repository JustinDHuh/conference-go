from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):  # for Location Picture - PEXEL
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": city + " " + state,
        "per_page": 1,
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # headers = {"Authorization": OPEN_WEATHER_API_KEY} # Don't need because its in the parameters.
    params = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    url_weather = "https://api.openweathermap.org/data/2.5/weather"
    # headers = {"Authorization": OPEN_WEATHER_API_KEY} # Don't need because API KEY is in parameters 'appid'
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(url_weather, params=params)
    content = json.loads(response.content)
    # print(content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None



    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
